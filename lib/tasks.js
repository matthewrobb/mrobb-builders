import "./tasks/app";
import "./tasks/dev";
import "./tasks/scripts";
import "./tasks/static";

import { define, call, wrap } from "./procs";

export { define, call, wrap };
